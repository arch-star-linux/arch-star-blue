# Arch Star Blue 

To be able to build you install this package

sudo pacman -S archiso


Download the last version.
Install with pacman -U nameofthefile

Do check out the archiso.readme.

Use the correct version of archiso.

Start building your own archstar version with the use of the scripts


# **update news from 2023 sep**

This project has ended, but I am now using Fedora OS and NixOS. You can access my dotfiles or configuration files here:

**nixos** :
    https://gitlab.com/krafi/nixos_config

**fedora**:
    https://gitlab.com/krafi/fedora-dotfile
    
**Please remember that my new configuration does not follow the minimal configuration philosophy."**
**If you have an old PC, you can still follow the Arch Star Linux config. **
**Additionally, you have the power to breathe new life into the Arch Star Linux project! 🌟**
