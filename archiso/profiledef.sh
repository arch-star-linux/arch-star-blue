#!/usr/bin/env bash
# shellcheck disable=SC2034

iso_name="Arch Star Blue"
iso_label="ARCH_STAR_Blue"
iso_publisher="Arch Star Linux <https://www.archstarlinux.com>"
iso_application="Arch Star Linux Live/Rescue CD"
iso_version="v1.1.5"
install_dir="arch"
buildmodes=('iso')
bootmodes=('bios.syslinux.mbr' 'bios.syslinux.eltorito' 'uefi-x64.systemd-boot.esp' 'uefi-x64.systemd-boot.eltorito')
arch="x86_64"
pacman_conf="pacman.conf"
airootfs_image_type="squashfs"
airootfs_image_tool_options=('-comp' 'xz' '-Xbcj' 'x86' '-b' '1M' '-Xdict-size' '1M')
file_permissions=(
  ["/etc/gshadow"]="0:0:0400"	
  ["/etc/shadow"]="0:0:0400"
  ["/root"]="0:0:750"
  ["/root/.automated_script.sh"]="0:0:755"
  ["/etc/polkit-1/rules.d"]="0:0:750"
  ["/etc/sudoers.d"]="0:0:750"
  ["/usr/local/bin/choose-mirror"]="0:0:755"
  ["/usr/local/bin/Installation_guide"]="0:0:755"
  ["/usr/local/bin/livecd-sound"]="0:0:755"
  ["/usr/local/bin/archstar-before"]="0:0:755"
  ["/usr/local/bin/archstar-displaymanager-check"]="0:0:755"
  ["/usr/local/bin/archstar-final"]="0:0:755"
  ["/etc/skel/.config/.archstar/swapf.sh"]="0:0:750"
  ["/etc/skel/.config/.archstar/fetch.sh"]="0:0:750"
  ["/etc/skel/.config/rofi/colorful/launcher.sh"]="0:0:750"
  ["/etc/skel/.config/polybar/scripts/battery-combined-shell.sh"]="0:0:750"
  ["/etc/skel/.config/polybar/scripts/battery-combined-tlp.sh"]="0:0:750"
  ["/etc/skel/.config/polybar/scripts/battery-combined-udev.sh"]="0:0:750"
  ["/etc/skel/.config/polybar/scripts/battery-cyberpower.sh"]="0:0:750"
  ["/etc/skel/.config/polybar/scripts/check-arch-updates.sh"]="0:0:750"
  ["/etc/skel/.config/polybar/scripts/check-aur-updates.sh"]="0:0:750"
  ["/etc/skel/.config/polybar/scripts/get_spotify_status.sh"]="0:0:750"
  ["/etc/skel/.config/polybar/scripts/inbox-imap-python.py"]="0:0:750"
  ["/etc/skel/.config/polybar/scripts/network-networkmanager.sh"]="0:0:750"
  ["/etc/skel/.config/polybar/scripts/pavolume.sh"]="0:0:750"
  ["/etc/skel/.config/polybar/scripts/pub-ip.sh"]="0:0:750"
  ["/etc/skel/.config/polybar/scripts/scroll_spotify_status.sh"]="0:0:750"
  ["/etc/skel/.config/polybar/scripts/spotify1.sh"]="0:0:750"
  ["/etc/skel/.config/polybar/scripts/tempcores.sh"]="0:0:750"
  ["/etc/skel/.config/polybar/scripts/weather.py"]="0:0:750"
  ["/etc/skel/.config/autostart/dex-autostart.sh"]="0:0:750"
  ["/etc/skel/.config/polybar/launch.sh"]="0:0:750"
  ["/etc/skel/.config/polybar/pacmancheck.sh"]="0:0:750"
)
